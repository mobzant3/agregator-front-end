module.exports = {
  siteUrl: "https://todaslastiendas.online/",
  generateRobotsTxt: true,
  outDir: "./out",
  exclude: ["/404"],
};
