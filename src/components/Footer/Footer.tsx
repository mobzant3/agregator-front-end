import Image from "next/image";
import { Text } from "tlt-ui-kit";
import Divider from "../common/Divider";
import footerStyles from "./footer.module.css";
import Link from "next/link";

const Footer = () => {
  return (
    <footer className={`flex flex-col ${footerStyles.footer}`}>
      <section className={`w-100 ${footerStyles.footer_content}`}>
        <section className="flex justify-between items-center -ml-5">
          <Image
            src="/tlt-logo-simple-blue.png"
            alt={"tlt-logo-simple"}
            width={113}
            height={62}
            quality={100}
          />
          <div className="h-8 border-2 flex justify-center items-center px-2  gap-1 rounded-3xl text-xs">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72" className="w-[3ch]">
              <path fill="#1e50a0" d="M5 17h62v38H5z"/>
              <path fill="#fff" d="M5 17h62v38H5z"/>
              <path fill="#61b2e4" d="M5 42h62v13H5zm0-25h62v13H5z"/>
              <path fill="#f1b31c" stroke="#f1b31c" strokeLinecap="round" strokeLinejoin="round" d="M36 33.897L37.236 32l-.06 2.299l2.06-.771l-1.334 1.822L40 36l-2.098.65l1.334 1.822l-2.06-.771l.06 2.299L36 38.103L34.764 40l.06-2.299l-2.06.771l1.334-1.822L32 36l2.098-.65l-1.334-1.822l2.06.771l-.06-2.299z"/>
              <path fill="none" stroke="#000" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 17h62v38H5z"/>
            </svg>
            <span className="select-none">ARS</span>
          </div>
        </section>
        <section className="flex flex-col justify-start gap-4 mb-7">
          <Link
            className="text-sm"
            href="/about-us"
          >
            Quiénes somos
          </Link>
          <Link
            className="text-sm"
            href="/publish-your-products"
          >
            Publica tus productos
          </Link>
          <Link
            className="text-sm"
            href="/terms"
          >
            Términos y condiciones
          </Link>
          </section>
        <Divider />
        <section className="mt-5 text-center">
          <Text fontSize={14} renderAs="a">
            TLT - Copyright 2023
          </Text>
        </section>
      </section>
    </footer>
  );
};

export default Footer;
