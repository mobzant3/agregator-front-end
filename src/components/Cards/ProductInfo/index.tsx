import { PromptInfo } from '@/types'

export default function ProductInfo({promptInfo}: {promptInfo: PromptInfo}) {
  if (!promptInfo.cleanBody && !promptInfo.combineBody && !promptInfo.opinionBody) return null;

  return (
    <section className="flex flex-wrap gap-40 my-20 mx-auto">
        {
            promptInfo.opinionBody && (
            <article className='flex-1'>
                <h2 className="text-4xl font-medium">Opiniones sobre...</h2>
                <p className="text-lg">{promptInfo.opinionBody}</p>
            </article>
            )
        }
        {promptInfo.opinionBody && 
            (
            <article className='flex-1'>
                <h2 className="text-4xl font-medium">
                    Con qué combinar...
                </h2>
                <p className="text-lg">{promptInfo.combineBody}</p>
            </article>
            )
        }
        {promptInfo.cleanBody && 
            <article className='flex-1'>
                <h2 className="text-4xl font-medium">Cómo limpiar...</h2>
                <p className="text-lg">{promptInfo.cleanBody}</p>
            </article>
        }
    </section>
  )
}
