import Image from "next/image";
import style from "./styles.module.css";

interface Props{
    images: string[];
    className?: string;
}

export default function ImageGallery({images, className}: Props) {
  return (
    <div className={`${style.ImageGallery} ${className}`}>
      <picture>
        <Image src={images[0]} width={386} height={386} alt="first image"/>
      </picture>
      <ul className={style.ImageGallery__MoreImages}>
        <li>
          <Image src={images[1] || images[0]} width={220} height={220} alt="second image"/>
        </li>
        <li>
          <Image src={images[2] || images[1] || images[0]} width={220} height={143} alt="three image"/>
        </li>
      </ul>
    </div>
  )
}
