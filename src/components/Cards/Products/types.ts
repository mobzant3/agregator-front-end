type Variant = {
  color: string;
  size: string;
  test: any;
};

export interface Product {
  _id: string;
  installments_data: string;
  product_id: number;
  price_short: string;
  url: string;
  price_long: string;
  price_number: number;
  compare_at_price_short: string;
  compare_at_price_long: string;
  compare_at_price_number: number;
  stock: number;
  sku: number;
  available: boolean;
  contact: boolean;
  id: number;
  image: number;
  name: string;
  indexable: boolean;
  shopName: string;
  indexableWords: string[];
  slugCombinations: string[];
  image_url: string;
  variants: Variant[];
  siteId: string;
  tags: string[];
  description: string;
  priority?: any;
}
