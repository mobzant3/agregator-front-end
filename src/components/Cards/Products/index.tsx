import React from "react";
import Image from "next/image";
import { Button, Text } from "tlt-ui-kit";
import cardStyles from "./product.module.css";

type ProductCardDescription = {
  imageUrl: string; // image_url
  price: string; // price_short
  shopName: string; // No lo tengo
  description: string;
};

const ProductCard = ({
  imageUrl,
  price,
  shopName,
  description,
}: ProductCardDescription) => {
  return (
    <div className="flex flex-col relative h-full w-full">
      <Image
        src={`https:${imageUrl}`}
        alt={"product-image"}
        className="aspect-square object-cover w-full"
        width={371}
        height={549}
        quality={100}
      />
      <div
        style={{
          background:
            "linear-gradient(180deg, rgba(255, 255, 255, 0) 0%, #FFFFFF 50%, #FFFFFF 56.77%)",
        }}
        className="flex flex-col pt-4"
      >
        <Button
          layoutComponentClasses="w-10 h-10 absolute top-3 right-3"
          modifier="outline"
          type="only-icon"
          onClick={() => null}
        >
          <Image
            src="/icons/Heart.svg"
            alt={"tlt-logo-simple"}
            width={20}
            height={18}
            quality={100}
          />
        </Button>
        <Text fontSize={25} fontWeight="bold">
          {price}
        </Text>
        <Text fontSize={20} fontWeight="bold">
          <span className="capitalize" style={{ color: "#B8B9BA" }}>
            {shopName}
          </span>
        </Text>
        <Text
          fontSize={20}
          layoutComponentClasses={`${cardStyles.description} text-ellipsis`}
        >
          {description}
        </Text>
      </div>
    </div>
  );
};

export default ProductCard;
