import Link from "next/link";
import React from "react";
import { Text } from "tlt-ui-kit";
import style from "./style.module.css"

type Props = {
  words: string[];
  currentCategory?: string;
  className?: string;
};

const BreadCrumb = ({ words, currentCategory, className }: Props) => {
  return (
    <section className={`flex justify-start items-center gap-2 capitalize ${style.BreadCrumb} ${className}`}>
      <Link href="/">
        Inicio
      </Link>
      { currentCategory && currentCategory != words[0] && 
          (
            <Text fontSize={20} fontWeight="medium">
              {currentCategory}
            </Text>
          )
      }
      {words.map((word, i) => {
        if(words.length - 1 === i){
          return (
            <Text key={i} fontSize={20} fontWeight="medium">
              {word}
            </Text>
          )
        }
        else{
          return (
            <Link href={`/${word}`} key={i}>
              {word}
            </Link>
          )
        }
      })}
    </section>
  );
};

export default BreadCrumb;
