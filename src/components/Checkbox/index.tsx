import { ChangeEvent, useState } from "react";
import { Checkbox as TltCheckbox } from "tlt-ui-kit";

interface props {
  label: string;
  value: string;
  layoutComponentClasses?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
}

export const Checkbox = ({ label, value, layoutComponentClasses, onChange }: props) => {
  const [isChecked, setIsChecked] = useState(false);

  const onClick = (event: ChangeEvent<HTMLInputElement>) => {
    setIsChecked(!isChecked);

    if (onChange) {
      onChange(event);
    }
  };

  return (
    <TltCheckbox
      isChecked={isChecked}
      onChange={onClick}
      label={label}
      layoutComponentClasses={layoutComponentClasses}
      value={value}
    />
  );
};
