import { Product } from "@/components/Cards/Products/types";

export const BaseSchema = () => ({
  __html: JSON.stringify({
    "@context": "https://schema.org/",
    "@type": "WebPage",
    name: "Todas las Tiendas",
    description: "TLT",
    url: "https://todaslastiendas.online",
  }),
});

export const SlugSchema = (slug: string) => ({
  __html: JSON.stringify({
    "@context": "https://schema.org/",
    "@type": "WebPage",
    name: slug.split("-").join(" "),
    description: "TLT",
    url: "https://todaslastiendas.online",
    breadcrumb: {
      "@type": "BreadcrumbList",
      itemListElement: [
        {
          "@type": "ListItem",
          position: 1,
          name: "Inicio",
          item: "http://www.todaslastiendas.online",
        },
        {
          "@type": "ListItem",
          position: 2,
          name: slug.split("-").join(" "),
          item: `http://www.todaslastiendas.online/${slug}`,
        },
      ],
    },
  }),
});

export const ProductSchema = (productDetail: Product) => ({
  __html: JSON.stringify({
    "@context": "https://schema.org/",
    "@type": "Product",
    name: productDetail.name,
    description: productDetail.description,
    image: productDetail.image_url,
    url: "https://todaslastiendas.online",
    sku: productDetail.sku,
    brand: {
      "@type": "Brand",
      name: productDetail.shopName,
    },
    offers: {
      "@type": "Offer",
      url: productDetail.url,
      priceCurrency: "ARS",
      price: productDetail.price_number,
      availability: "https://schema.org/InStock",
      inventoryLevel: {
        "@type": "QuantitativeValue",
        value: productDetail.stock,
      },
      seller: {
        "@type": "Organization",
        name: productDetail.shopName,
      },
    },
  }),
});

const Schemas = { BaseSchema, SlugSchema, ProductSchema };

export default Schemas;
