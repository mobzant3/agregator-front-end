import Head from "next/head";
import { FC, useEffect, useState } from "react";
import Footer from "../../Footer/Footer";
import layoutStyles from "./layout.module.css";
import Image from "next/image";
import SearchResultInput from "../../SearchResultInput/SearchResultInput";
import LayoutContext from "../../../contexts/LayoutContext";
import BreadCrumb from "../../BreadCrumb/BreadCrumb";
import useKeyDown from "../../../customhooks/useKeyDown";
import { useRouter } from "next/router";
import getPageByProximity from "../../../services/fuseService";
import { SearchIcon } from "@/components/SearchIcon/SearchIcon";

interface LayoutProps {
  children: React.ReactNode;
  slug?: string;
  className?: string;
  withSearchBar?: boolean;
  withHeader?: boolean;
  headTitle?: string;
}

const CustomLayout: FC<LayoutProps> = ({
  children,
  withSearchBar = true,
  withHeader = true,
  slug,
  className,
  headTitle = "TLT",
}) => {
  const [openSearchBar, setOpenSearchBar] = useState(false);

  const [searchParams, setSearchParams] = useState<string[] | undefined>(
    undefined
  );
  const [searchResultInput, setSearchResultInput] = useState("");

  const router = useRouter();
  const handleKeyPress = (event: KeyboardEvent) => {
    if (event.key === "Enter" && searchResultInput) {
      event.preventDefault();
      const path = getPageByProximity({ query: searchResultInput });
      router.push(`/${path}`);
    }
  };

  useKeyDown(handleKeyPress, searchResultInput);

  const toggleSearchBar = () => {
    setOpenSearchBar(!openSearchBar);
  };

  const handleInputChange = (value: any) => {
    setSearchResultInput(value.target.value);
  };

  const goToHome = () => {
    router.push("/");
  };

  useEffect(() => {
    if (slug) {
      const wordList = slug.split("-");
      setSearchParams(wordList);
    } else {
      const url = new URL(location.href);
      const searchParamsFromUrl = url.searchParams.get("searchParams") || "";
      setSearchParams(searchParamsFromUrl.split("-"));
    }
  }, [slug]);

  return (
    <>
      <Head>
        <title>{headTitle}</title>
        <meta name="description" content="TLT" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <section className={layoutStyles.container}>
        <LayoutContext.Provider value={{ searchParams }}>
          <main className={`${layoutStyles.main} relative ${className}`}>
            {withHeader && (
              <div
                className="flex flex-wrap w-max items-center justify-between"
                style={{ width: "100%" }}
              >
                <Image
                  onClick={goToHome}
                  src="/tlt-logo-simple-blue.png"
                  alt={"tlt-logo-simple"}
                  className="-ml-7 -mr-16 z-10 cursor-pointer"
                  width={182}
                  height={100}
                  quality={100}
                />
                {withSearchBar && (
                  <>
                    <div
                      className="absolute bg-white"
                      style={{ width: 400, height: 40, left: 49 }}
                    ></div>
                    <SearchIcon
                      iconSize={22}
                      onClick={() => toggleSearchBar()}
                      className="cursor-pointer z-10"
                    />
                    {openSearchBar && (
                      <SearchResultInput
                        placeholder={searchParams?.join(" ")}
                        onChange={handleInputChange}
                        iconSize={10}
                        layoutComponentClasses={layoutStyles.slug_search_input}
                      />
                    )}
                  </>
                )}
              </div>
            )}
            {children}
          </main>
          <Footer />
        </LayoutContext.Provider>
      </section>
    </>
  );
};

export default CustomLayout;
