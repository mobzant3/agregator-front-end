import Link from "next/link";
import { Text } from "tlt-ui-kit";

type iProps = {
  SimilarSearchs?: string[];
  variant?: "underline" | "default" | undefined
  fontSize?: string | number
};

export default function SimilarSearchs({ SimilarSearchs, variant, fontSize }: iProps) {
  return (
    <div>
      <div className="flex flex-col gap-3">
        <Text renderAs="h3" fontWeight="normal" fontSize={20}>
          Busquedas Similares
        </Text>
        <div className="flex flex-col gap-1">
          {SimilarSearchs &&
            SimilarSearchs.map((search) => (
              <Link href={`/${search}`} key={search}>
                <Text
                  key={search}
                  renderAs="p"
                  layoutComponentClasses="underline underline-offset-4 capitalize"
                  variant={variant}
                  fontSize={fontSize || 16}
                >
                  {search.split("-").join(" ")}
                </Text>
              </Link>
            ))}
        </div>
      </div>
    </div>
  );
}
