import { CategoriesPrototype, CategoriesItem } from "@/types";
import Link from "next/link";
import { Text } from "tlt-ui-kit";

type CategoriesProps = {
  categories: CategoriesPrototype;
  variant?: "underline" | "default" | undefined
  fontSize?: string | number
};

const Categories = ({ categories, variant, fontSize }: CategoriesProps) => {
  const allCategories: CategoriesItem[] = [];
  const CategoriesInArray = Object.keys(categories).reduce((acc, type) => {
    return acc.concat(categories[type]);
  }, allCategories);

  return (
    <div>
      <div className="flex flex-col gap-3">
        <Text renderAs="h3" fontWeight="normal" fontSize={20}>
          Categorías más populares
        </Text>
        <div className="flex flex-col gap-1">
          {categories &&
            CategoriesInArray.flatMap((cat, i) => (
              <Link href={`/${cat.url}`} key={i}>
                <Text
                  renderAs="p"
                  layoutComponentClasses="underline underline-offset-4 lowercase first-letter:uppercase"
                  variant={variant}
                  fontSize={fontSize || 16}
                >
                  {cat.value}
                </Text>
              </Link>
            ))
          }
        </div>
      </div>
    </div>
  );
};

export default Categories;
