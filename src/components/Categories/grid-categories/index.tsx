import { CategoriesItem } from "@/types";
import Link from "next/link";
import style from "./style.module.css"

interface Props {
  items: CategoriesItem[];
  className?: string;
}

export default function GridCategories({items, className}: Props) {
  return (
    <article className={`${style.StackCategories} ${className}`}>
        {items.map((item, index) => (
            <Link href={item.url} key={index}>{item.value}</Link>
        ))}
    </article>
  )
}
