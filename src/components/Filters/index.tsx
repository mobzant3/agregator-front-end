import { Text } from "tlt-ui-kit";
import { Checkbox } from "@/components/Checkbox";
import { filterParams, filterPrice } from "@/types";
import { ChangeEvent, Dispatch, SetStateAction } from "react";

interface Props{
  layoutComponentClasses?: string;
  setFilterParams: Dispatch<SetStateAction<filterParams>>;
}

export const Filters = ({
  layoutComponentClasses,
  setFilterParams,
}: Props) => {
  const onPriceChange = (event: ChangeEvent<HTMLInputElement>, range: filterPrice) => {
    if (event.target.checked) {
      setFilterParams((prevFilters) => ({
        ...prevFilters,
        price: {
          min: range.min,
          max: range.max,
        },
      }));
    } else {
      setFilterParams((prevFilters) => ({
        ...prevFilters,
        price: {
          min: null,
          max: null,
        },
      }));
    }
  }

  const onColorChange = (event: ChangeEvent<HTMLInputElement>) => {
    const colorValue = event.target.value;

    setFilterParams((prevFilters) => {
      const newColorSet = new Set(prevFilters.color);
      if (event.target.checked) {
        newColorSet.add(colorValue);
      } else {
        newColorSet.delete(colorValue);
      }
      return { ...prevFilters, color: newColorSet };
    });
  };
  
  return (
    <div className={layoutComponentClasses}>
      <div className="flex flex-col gap-2">
        <Text fontSize={20}>Precio</Text>
        <ul className="grid gap-1">
          <li>
            <Checkbox label="$0 - $1.000" value="0-1000" onChange={(event) => onPriceChange(event, {min: 0, max: 1000})}/>
          </li>
          <li>
            <Checkbox label="$1.000 - $5.000" value="1000-5000" onChange={(event) => onPriceChange(event, {min: 1000, max: 5000})}/>
          </li>
          <li>
            <Checkbox label="$5.000 - $10.000" value="5000-10000" onChange={(event) => onPriceChange(event, {min: 5000, max: 10000})}/>
          </li>
          <li>
            <Checkbox label="+ $10.000" value="+10000" onChange={(event) => onPriceChange(event, {min: 10000})}/>
          </li>
        </ul>
      </div>
      <div className="flex flex-col gap-2">
        <Text fontSize={20}>Color</Text>
        <ul className="flex flex-col gap-1">
          <li>
            <Checkbox label="Negro" value="negro" onChange={(event) => onColorChange(event)} />
          </li>
          <li>
            <Checkbox label="Azul" value="azul" onChange={(event) => onColorChange(event)} />
          </li>
          <li>
            <Checkbox label="Rojo" value="rojo" onChange={(event) => onColorChange(event)} />
          </li>
          <li>
            <Checkbox label="Blanco" value="blanco" onChange={(event) => onColorChange(event)} />
          </li>
        </ul>
      </div>
    </div>
  );
};
