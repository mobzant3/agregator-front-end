import React from "react";
import Image from "next/image";
import Colors from "../../constants/colors";

export type SearchIconProps = {
  iconSize?: number;
  className?: string;
  onClick?: () => void;
};

export const SearchIcon = ({
  iconSize,
  className,
  onClick,
}: SearchIconProps) => {
  return (
    <div
      className={`rounded-full p-2 ${className}`}
      style={{ background: Colors.secondaryGreen }}
      onClick={onClick}
    >
      <Image
        width={iconSize ? iconSize : 22}
        height={iconSize ? iconSize : 22.34}
        src="/icons/Magnifying glass.svg"
        alt="Magnifying glass"
      />
    </div>
  );
};
