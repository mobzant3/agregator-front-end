import React from "react";
import { Input } from "tlt-ui-kit";
import searchInputStyle from "./search_input.module.css";
import { SearchIcon } from "../SearchIcon/SearchIcon";

type SearchResultInputProps = {
  onChange: any;
  layoutComponentClasses?: string;
  placeholder?: string;
  iconSize?: number;
  onClickIcon?: () => void;
};

const SearchResultInput = ({
  onChange,
  placeholder,
  layoutComponentClasses = "",
  iconSize,
  onClickIcon,
}: SearchResultInputProps) => {
  return (
    <Input
      type="text"
      placeholder={placeholder || "Buscar"}
      borderVariant="secondary-green"
      iconElement={<SearchIcon iconSize={iconSize} onClick={onClickIcon} />}
      handleChange={onChange}
      layoutComponentClasses={`${searchInputStyle.search_input} ${layoutComponentClasses}`}
    />
  );
};

export default SearchResultInput;
