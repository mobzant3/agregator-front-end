import { useEffect, useState } from "react";

export default function useTimer(time: number) {
  const [hasTimerFinished, setHasTimerFinished] = useState(false);

  useEffect(() => {
    const timeOut = setTimeout(() => {
      setHasTimerFinished(true);
    }, time);

    return () => {
      clearTimeout(timeOut);
    };
  }, [time]);

  return [hasTimerFinished];
}
