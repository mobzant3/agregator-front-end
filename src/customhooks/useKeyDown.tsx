import { useEffect } from "react";

export default function useKeyDown(
  callback: (event: KeyboardEvent) => void,
  dependantState: any
) {
  useEffect(() => {
    document.addEventListener("keydown", callback, false);
    return () => {
      document.removeEventListener("keydown", callback, false); // Succeeds
    };
  }, [callback, dependantState]);

  return;
}
