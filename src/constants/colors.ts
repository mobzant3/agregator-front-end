const Colors = {
  primaryBlue: "#1a7de8",
  primaryBlack: "#0e0f11",
  primaryWhite: "#fdfdfd",

  /* Grey */
  greyPrimary: "#323334",
  greyMedium: "#b8b9ba",
  greyLighter: "#dfe0e1",

  /* Secondary colors */
  secondaryGreen: "#d1f452",
  secondaryGrey: "#e7dbd9",

  /* State colors */
  successGreen: "#d3f2df",
  warningOrange: "#f4e38e",
  errorRed: "#f4724a",

  /* Other colors */
  transparent: "transparent",
};

export default Colors