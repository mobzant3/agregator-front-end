import Paths from "../../json/paths.json";
import Fuse from "fuse.js";
const { Index } = require("flexsearch");

const index = new Index({
  tokenize: "full",
  context: {
    encode: "extra",
  },
});

Paths.forEach((x, id) => {
  index.add(id, x);
});

const fuse = new Fuse(Paths as any[], {
  isCaseSensitive: false,
  includeScore: true,
  shouldSort: true,
  ignoreLocation: true,
});

type ProximitySearchParams = {
  query: string;
};

const getPageByProximity = ({ query }: ProximitySearchParams) => {
  const resultIndexes = index.search(query);
  if (resultIndexes[0]) {
    return Paths[resultIndexes[0]];
  }

  const searchResult = fuse.search(query);
  return searchResult[0].item;
};

export default getPageByProximity;
