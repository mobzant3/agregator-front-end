import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";
import SearchResultInput from "../components/SearchResultInput/SearchResultInput";
import CustomLayout from "../components/common/Layout/Layout";
import useKeyDown from "@/customhooks/useKeyDown";
import getPageByProximity from "@/services/fuseService";
import GridCategories from "@/components/Categories/grid-categories";
import { CategoriesPrototype } from "@/types";
import { Text } from "tlt-ui-kit";
import { filterSlugsArrayWithoutProducts } from "@/utils";
import Head from "next/head";
import Schemas from "@/components/common/SchemaMarckup";

type HomeProps = {
  categories: CategoriesPrototype;
};

const Home = ({ categories }: HomeProps) => {
  const [searchResultInput, setSearchResultInput] = useState("");
  const router = useRouter();

  const handleKeyPress = (event: KeyboardEvent) => {
    if (event.key === "Enter" && searchResultInput) {
      event.preventDefault();
      const path = getPageByProximity({ query: searchResultInput });
      router.push(`/${path}`);
    }
  };

  const onClickIcon = () => {
    if (searchResultInput) {
      const path = getPageByProximity({ query: searchResultInput });
      router.push(`/${path}`);
    }
  };

  useKeyDown(handleKeyPress, searchResultInput);

  const handleInputChange = (value: any) => {
    setSearchResultInput(value.target.value);
  };

  return (
    <CustomLayout headTitle="Todas Las Tiendas Online de Argentina" withSearchBar={false} withHeader={false} className="container mx-auto">
      <Head>
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={Schemas.BaseSchema()}
        />
      </Head>
      <section className="flex justify-center">
        <Image
          src="/tlt-logo-color.png"
          alt={"Zig-zag-image"}
          width={181}
          height={141}
          quality={100}
        />
      </section>
      <section className="gap-6 flex flex-col">
        <SearchResultInput onChange={handleInputChange} onClickIcon={onClickIcon} />
      </section>
      <section className="flex flex-col gap-10 mt-10 text-lgon">
        <article className="grid grid-cols-10 gap-10">
          <Image src="/cards/Ropa.svg" width={500} height={370} alt="Ropa-Category-Card" className="col-span-4 object-cover rounded-[40px] my-auto max-lg:col-span-full max-lg:h-36 max-lg:w-full"/>
          <GridCategories items={categories.ropa} className="col-span-6 w-full [place-self:start_end] h-full bg-[#F4EFEE] rounded-[40px] max-lg:col-span-full" />
        </article>
        <article className="grid grid-cols-10 gap-10">
          <GridCategories items={categories.accesorios} className="col-span-6 place-self-start w-full h-full bg-[#F4EFEE] rounded-[40px] max-lg:col-span-full max-lg:order-2" />
          <Image src="/cards/Accesorios.svg" width={500} height={370} alt="Ropa-Category-Card" className="col-span-4 place-self-end object-cover rounded-[40px] my-auto max-lg:col-span-full max-lg:h-36 max-lg:w-full max-lg:order-1"/>
        </article>
        <article className="grid grid-cols-10 gap-10">
          <Image src="/cards/Calzado.svg" width={500} height={370} alt="Ropa-Category-Card" className="col-span-4 object-cover rounded-[40px] my-auto max-lg:col-span-full max-lg:h-36 max-lg:w-full"/>
          <GridCategories items={categories.calzado} className="col-span-6 [place-self:start_end] w-full h-full bg-[#F4EFEE] rounded-[40px] max-lg:col-span-full max-lg:w-full" />
        </article>
      </section>
      <section className="flex flex-col gap-10 py-32 text-center rou">
        <Text fontSize={22}>
          ¿Estás buscando dónde comprar productos de tiendas online en Argentina? Llegaste al lugar indicado :)
        </Text>
        <Text fontSize={22}>
          Todas Las Tiendas Online te permite encontrar todo tipo de productos, incluido ropa, calzado y accesorios.
        </Text>
      </section>
    </CustomLayout>
  );
};

export async function getStaticProps() {
  const categories = await fetch(
    `http://localhost:3000/categories/product-types`
  ).then((res) => {
    return res.json();
  }).then((res) => {
    return res.data
  });

  // filter categories without products
  for(let category in categories){
    categories[category] = await filterSlugsArrayWithoutProducts(categories[category]);
  }

  return {
    props: { categories },
  };
}

export default Home;
