import CustomLayout from "../components/common/Layout/Layout";

const Custom404 = () => {
  return (
    <CustomLayout headTitle="TLT - 404">
      <h1 className="m-auto">No encontramos lo que estaba buscando :(</h1>
    </CustomLayout>
  );
};

export default Custom404;
