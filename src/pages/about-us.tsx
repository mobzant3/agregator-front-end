import CustomLayout from "../components/common/Layout/Layout";
import { Text } from "tlt-ui-kit";

const AboutUs = () => {
  return (
    <CustomLayout headTitle="Quiénes Somos" >
      <section className="flex flex-col gap-10 py-5 pb-10">
        <article>
          <Text fontSize={36} fontWeight="bold" layoutComponentClasses="block">Quiénes Somos</Text>
          <Text fontSize={18}>En TLT, somos una fuerza impulsora detrás del éxito de tu tienda online. Como parte de la empresa de desarrollo de software BeWise (https://www.bewise.com.es/), nuestra misión es potenciar negocios online y llevarlos al siguiente nivel.</Text>
        </article>

        <article>
          <Text fontSize={36} fontWeight="bold" layoutComponentClasses="block">Nuestra Pasión</Text>
          <Text fontSize={18}>Nos apasiona el mundo del comercio electrónico y entendemos que el tráfico de calidad es esencial para el éxito de cualquier tienda online. Es por eso que nos especializamos en generar tráfico altamente segmentado y relevante para tu negocio. Sabemos que cada tienda es única, y trabajamos para crear estrategias personalizadas que se adapten a tus necesidades y objetivos específicos.</Text>
        </article>

        <article>
          <Text fontSize={36} fontWeight="bold" layoutComponentClasses="block">Nuestro Enfoque</Text>
          <Text fontSize={18}>Nuestro enfoque se basa en la combinación de experiencia en desarrollo de software y conocimientos avanzados en marketing digital. Utilizamos tecnología de vanguardia y estrategias probadas de SEO para atraer visitantes a tu tienda y convertirlos en clientes satisfechos. </Text>
        </article>

        <article>
          <Text fontSize={36} fontWeight="bold" layoutComponentClasses="block">Equipo de Expertos</Text>
          <Text fontSize={18}>Contamos con un equipo de expertos en marketing digital, analítica web, y desarrollo de software altamente capacitados y apasionados por lo que hacen. Están dedicados a mantenerte un paso adelante de la competencia y a garantizar que tu tienda en línea alcance su máximo potencial.</Text>
        </article>

        <article>
          <Text fontSize={36} fontWeight="bold" layoutComponentClasses="block">Nuestra Promesa</Text>
          <Text fontSize={18}>En TLT, no solo generamos tráfico, sino que construimos relaciones duraderas. Nos comprometemos a trabajar de la mano contigo, comprendiendo tus metas y desafíos, para proporcionarte soluciones efectivas y medibles que impulsen el crecimiento sostenible de tu negocio en línea.</Text>
        </article>

        <article>
          <Text fontSize={18}>Contáctanos hoy mismo en hola@todaslastiendas.online y descubre cómo podemos impulsar tu tienda en línea hacia el éxito.</Text>
        </article>
      </section>
    </CustomLayout>
  )
}

export default AboutUs
