import CustomLayout from "../components/common/Layout/Layout";
import { Text } from "tlt-ui-kit";

const PublishYourProducts = () => {
  return (
    <CustomLayout headTitle="Publica tus productos" >
      <section className="flex flex-col gap-10 py-5 pb-10">
        <article>
          <Text fontSize={36} fontWeight="bold" layoutComponentClasses="block">Si queres publicar tus productos en Todas Las Tiendas</Text>
          <Text fontSize={20}>Comunicate con nosotros en </Text>
          <a href="mailto:hola@todaslastiendas.online" rel="noopener noreferrer" className="underline text-xl" target="_blank">hola@todaslastiendas.online</a>
        </article>
      </section>
    </CustomLayout>
  )
}

export default PublishYourProducts
