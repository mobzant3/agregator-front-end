import Image from "next/image";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import { Button, Text } from "tlt-ui-kit";
import ProductCard from "../components/Cards/Products";
import CustomLayout from "../components/common/Layout/Layout";
import LayoutContext from "../contexts/LayoutContext";
import Categories from "@/components/Categories/Categories";
import { Filters } from "@/components/Filters";
import { CategoriesPrototype, filterParams, Product, PromptInfo } from "@/types";
import { filterProducts, filterSlugsArrayWithoutProducts } from "@/utils";
import SimilarSearchs from "@/components/SimilarSearchs/SimilarSearchs";
import Link from "next/link";
import BreadCrumb from "@/components/BreadCrumb/BreadCrumb";
import ProductInfo from "@/components/Cards/ProductInfo";
import Head from "next/head";
import Schemas, { SlugSchema } from "@/components/common/SchemaMarckup";

const PRODUCTS_URL = "http://localhost:3000/products";
const SLUGS_URL = "http://localhost:3000/slugs";

export default function HomeWithSearchParams({
  slug,
  products,
  total,
  categories,
  currentCategory,
  similarSearchs,
  promptInfo
}: {
  slug: string;
  products: Product[];
  total: number;
  categories: CategoriesPrototype;
  currentCategory: string;
  similarSearchs: string[];
  promptInfo: PromptInfo;
}) {
  const [searchLimit, setSearchLimit] = useState(16);

  const [headerTitle, setHeaderTitle] = useState<string>("");

  const [showFilters, setShowFilters] = useState(false);

  const [filterParams, setFilterParams] = useState<filterParams>({
    price: { min: null, max: null },
    color: new Set<string>(),
  });

  const router = useRouter();

  const handleClick = async () => {
    setSearchLimit((value) => {
      return (value = value + 16);
    });
  };

  const onFilterClick = () => {
    setShowFilters(!showFilters);
  };

  const filteredProducts:Product[] = filterProducts(products, filterParams);

  const wordList = slug.split("-");

  useEffect(() => {
    setHeaderTitle(
      `TLT - ${wordList
        .join(" ")
        .charAt(0)
        .toUpperCase()
        .concat(wordList.join(" ").slice(1))}`
    );
  }, [wordList, slug]);

  const { searchParams } = useContext(LayoutContext);

  if(showFilters){
    return (
      <CustomLayout headTitle={headerTitle}>
        <Head>
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={Schemas.SlugSchema(
              slug
            )}
          />
        </Head>
        <div className="fixed w-full h-full bg-white top-0 bottom-0 left-0 right-0 z-50 p-16 flex flex-col overflow-y-auto">
          <span
            onClick={() => onFilterClick()}
            className="absolute top-6 left-6 cursor-pointer"
          >
            <Image
              src="/icons/Close.svg"
              alt={"tlt-logo-simple"}
              width={20}
              height={18}
              quality={100}
            />
          </span>
          <Text
            fontSize={30}
            fontWeight="bold"
            layoutComponentClasses="mt-3 mb-14"
          >
            Filtrar por
          </Text>
          <Filters layoutComponentClasses="flex flex-col gap-10" setFilterParams={setFilterParams} />
        </div>
      </CustomLayout>
    )
  }
  
  return (
    <CustomLayout slug={slug} headTitle={headerTitle}>
      <Head>
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={Schemas.SlugSchema(
            slug
          )}
        />
      </Head>
      <BreadCrumb words={wordList} currentCategory={currentCategory}/>
      <section>
        <section>
          <Text fontSize={40} fontWeight="bold">
            <h1 className="capitalize">{slug.split("-").join(" ")}</h1>
          </Text>
          <section className="flex justify-between items-center gap-3 my-7 h-14">
            <Text
              fontSize={20}
              layoutComponentClasses="text-[var(--primary-blue)]"
            >{`${filteredProducts?.length} resultados`}</Text>
            <div className="lg:hidden">
              <Button
                layoutComponentClasses="w-14 bg-red-400"
                modifier="outline"
                type="only-icon"
                onClick={() => onFilterClick()}
              >
                <Image
                  src="/icons/Adjust.svg"
                  alt={"tlt-logo-simple"}
                  width={20}
                  height={18}
                  quality={100}
                />
              </Button>
            </div>
          </section>
        </section>
        <div className="lg:grid lg:grid-cols-[1fr_3fr] lg:auto-rows-min lg:gap-6">
          <aside className="flex flex-col gap-7 lg:row-start-1 lg:row-end-auto max-lg:hidden">
            <Filters setFilterParams={setFilterParams}/>
          </aside>
          <section className="grid grid-cols-[repeat(auto-fill,minmax(15rem,1fr))] place-items-center gap-12 lg:col-start-2 lg:col-end-3 lg:row-start-1 lg:row-end-4">
            {filteredProducts?.slice(0, searchLimit).map((product, i) => (
              <Link
                href={`/products/${product._id}?searchParams=${slug}`}
                className="cursor-pointer h-full w-full flex"
                key={i}
              >
                <ProductCard
                  imageUrl={product.image_url}
                  price={product.price_short}
                  shopName={product.shopName}
                  description={product.description}
                />
              </Link>
            ))}
          </section>
          <section className="w-full flex justify-center mt-14 mb-6 lg:order-4  lg:col-start-2 lg:col-end-3 lg:mt-0 lg:mb-14">
            <Button
              layoutComponentClasses="h-14 border-8"
              modifier="outline"
              onClick={handleClick}
            >
              <Text fontSize={20} fontWeight="bold">
                ver más
              </Text>
            </Button>
          </section>
          <section className="lg:order-3 lg:row-start-2 lg:row-end-auto grid gap-10">
            <SimilarSearchs SimilarSearchs={similarSearchs} />
            <Categories categories={categories} />
          </section>
        </div>
      </section>
      <ProductInfo promptInfo={promptInfo}/>
    </CustomLayout>
  );
}

export async function getStaticPaths() {
  const path = require("path");
  const jsonDirectory = path.join(process.cwd(), "json");
  const fs = require("fs");
  const pathsJson = require("../../json/paths.json");

  let finalSlugs = [];

  if (pathsJson.length) {
    finalSlugs = pathsJson;
  } else {
    const request = await fetch(SLUGS_URL);
    const { slugs } = await request.json();

    finalSlugs = slugs;

    fs.writeFileSync(
      `${jsonDirectory}/paths.json`,
      JSON.stringify(slugs, null, 4)
    );
  }

  const paths = finalSlugs.map((slug: string) => ({ params: { slug: slug } }));

  console.log("loading...");

  return {
    paths,
    fallback: false, // can also be true or 'blocking'
  };
}

export async function getStaticProps(context: { params: any }) {
  const { params } = context;
  const slug = params?.slug || "";

  const request = await fetch(
    `${PRODUCTS_URL}?slug=${slug}&limit=1000&skip=0`
  );
  const { data, total } = await request.json();

  const categories = await fetch(
    `http://localhost:3000/categories/product-types`
  ).then((res) => {
    return res.json();
  }).then((res) => {
    return res.data
  });

  // filter categories without products
  for(let category in categories){
    categories[category] = await filterSlugsArrayWithoutProducts(categories[category]);
  }

  const currentCategory: string = await fetch(
    `http://localhost:3000/categories/product-types/${slug}`
  ).then((res) => {
    return res.json();
  }).then((res) => {
    return res.data
  });

  const similarSearchs = await fetch(
    `http://localhost:3000/slugs/similar-searchs?slug=${slug}`
  ).then((res) => {
    return res.json();
  }).then((res) => {
    return res.data
  });
  
  const promptInfo = await fetch(
    `http://localhost:3000/slugs/search-prompt-info?slug=${slug}`
  ).then((res) => {
    return res.json();
  }).then((res) => {
    return res.data
  });

  return {
    props: { slug: slug, products: data, total, categories, currentCategory, similarSearchs, promptInfo },
  };
}
