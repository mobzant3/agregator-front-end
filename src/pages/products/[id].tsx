import { useRouter } from "next/router";
import CustomLayout from "../../components/common/Layout/Layout";
import ProductCard from "../../components/Cards/Products";
import { Product } from "../../components/Cards/Products/types";
import { Button, Text } from "tlt-ui-kit";
import productDetailStyles from "../../styles/pages/productDetail.module.css";
import Head from "next/head";
import Schemas from "@/components/common/SchemaMarckup";
import ImageGallery from "@/components/Cards/ImageGallery";
import BreadCrumb from "@/components/BreadCrumb/BreadCrumb";
import Image from "next/image";

export type ProductDetail = {
  productDetail: Product;
};

const ProductDetail = ({ productDetail }: ProductDetail) => {
  const { push, query } = useRouter();
  const { searchParams } = query;

  if (!productDetail) {
    return <h1>Loading...</h1>;
  }

  function handleClick(): void {
    push(
      {
        pathname: "/redirect", // Ruta de la página
        query: {
          url: `${productDetail.url}`,
          product_id: `${productDetail._id}`,
        }, // Consultas que desees agregar
      },
      `/redirect?url=${productDetail.url}`
    ); // La URL visible en el navegador
  }

  return (
    <CustomLayout headTitle={productDetail.name} className="container mx-auto">
      <Head>
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={Schemas.ProductSchema(
            productDetail
          )}
        />
      </Head>
      {searchParams && <BreadCrumb words={String(searchParams).split("-")} className="lg:pb-5"/>}
      <section className="grid gap-[0_2.5rem] mb-20 lg:justify-between lg:grid-cols-[3fr_2fr] lg:grid-rows-[auto_auto_1fr_auto]">
        <Text renderAs="h1" fontSize={37} fontWeight="bold" layoutComponentClasses="capitalize lg:order-2">{productDetail.name}</Text>
        <ImageGallery images={[productDetail.image_url]} className="lg:order-1 lg:row-span-3"/>
        <div className="flex w-full justify-between lg:flex-col max-lg:mt-5 lg:order-3">
          <Text fontSize={27} fontWeight="bold">{productDetail.price_short}</Text>
          <Button
            layoutComponentClasses="w-12 h-12 lg:my-5"
            modifier="outline"
            type="only-icon"
            onClick={() => null}
          >
            <Image
              src="/icons/Heart.svg"
              alt={"tlt-logo-simple"}
              width={20}
              height={18}
              quality={100}
            />
          </Button>
        </div>
        <article className="order-5">
          <Text renderAs="h2" fontSize={16} fontWeight="bold" layoutComponentClasses="text-[#B8B9BA]">{productDetail.shopName}</Text>
          <Text renderAs="p" fontSize={18}>{productDetail.description}</Text>
        </article>
        <Button
          layoutComponentClasses={`border-8 h-16 mt-5 w-full max-lg:mx-auto lg:order-4`}
          onClick={handleClick}
        >
          <Text fontSize={20} fontWeight="bold">
            comprar
          </Text>
        </Button>
      </section>
    </CustomLayout>
  );
};

export default ProductDetail;

export async function getStaticPaths() {
  const request = await fetch("http://localhost:3000/products");
  const { data } = await request.json();

  const paths = [] as any;
  data.forEach((post: { _id: string }) => {
    paths.push({ params: { id: post._id } });
  });

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps(context: { params: any }) {
  const {
    params: { id },
  } = context;

  const request = await fetch(`http://localhost:3000/products/${id}`);
  
  const productDetail = await request.json();

  return {
    props: { productDetail },
  };
}
