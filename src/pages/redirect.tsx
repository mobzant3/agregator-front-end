import { useRouter } from "next/router";
import React, { useEffect } from "react";
import useTimer from "../customhooks/useTimer";
import CustomLayout from "../components/common/Layout/Layout";

const Redirect = () => {
  const [hasTimerFinished] = useTimer(1000);

  const { query } = useRouter();

  useEffect(() => {
    if (hasTimerFinished) {
      window.location.replace(query.url as string);
    }
  }, [hasTimerFinished, query]);

  if (!hasTimerFinished) {
    return (
      <CustomLayout withHeader={true}>
        <h1>Loading....</h1>
      </CustomLayout>
    );
  }

  return (
    <CustomLayout withHeader={true}>
      <h1>Redirecting...</h1>
    </CustomLayout>
  );
};

export default Redirect;
