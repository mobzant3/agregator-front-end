import CustomLayout from "../components/common/Layout/Layout";
import { Text } from "tlt-ui-kit";

const Terms = () => {
  return (
    <CustomLayout headTitle="Términos y Condiciones de Uso" >
      <section className="flex flex-col gap-10 py-5 pb-10">
        <article className="grid gap-3">
            <Text fontSize={47} fontWeight="bold">Términos y Condiciones de Uso</Text>
            <Text fontSize={18} fontWeight="bold">Fecha de entrada en vigencia: Octubre 2023</Text>
            <Text fontSize={18}>Por favor, lea atentamente estos términos y condiciones antes de utilizar nuestro sitio web y nuestros servicios. Al acceder o utilizar este sitio web, usted acepta y está de acuerdo con estos términos y condiciones. Si no está de acuerdo con alguno de los siguientes términos, le recomendamos que no utilice nuestro sitio web ni nuestros servicios.</Text>
        </article>

        <article>
            <Text fontSize={28} fontWeight="bold" layoutComponentClasses="block">1. Uso Adecuado</Text>
            <Text fontSize={18}>1.1. Usted se compromete a utilizar nuestro sitio web y servicios de acuerdo con todas las leyes y regulaciones aplicables en Argentina.</Text>
            <Text fontSize={18}>1.2. No está permitido utilizar nuestro sitio web con fines ilegales o no autorizados.</Text>
        </article>

        <article>
            <Text fontSize={28} fontWeight="bold" layoutComponentClasses="block">2. Privacidad</Text>
            <Text fontSize={18}>2.1. Su privacidad es importante para nosotros. Lea nuestra Política de Privacidad para comprender cómo recopilamos, utilizamos y protegemos sus datos personales.</Text>
        </article>

        <article>
            <Text fontSize={28} fontWeight="bold" layoutComponentClasses="block">3. Propiedad Intelectual</Text>
            <Text fontSize={18}>3.1. Todo el contenido en este sitio web, incluyendo pero no limitado a textos, gráficos, logotipos, imágenes, videos y software, está protegido por derechos de autor y otras leyes de propiedad intelectual en Argentina.</Text>
            <Text fontSize={18}>3.2. Usted acepta no copiar, reproducir, distribuir o utilizar el contenido de este sitio web sin nuestro permiso por escrito.</Text>
        </article>

        <article>
            <Text fontSize={28} fontWeight="bold" layoutComponentClasses="block">4. Enlaces a Terceros</Text>
            <Text fontSize={18}>4.1. Nuestro sitio web puede contener enlaces a sitios web de terceros. No tenemos control sobre el contenido o las políticas de privacidad de estos sitios y no somos responsables de ellos.</Text>
        </article>

        <article>
            <Text fontSize={28} fontWeight="bold" layoutComponentClasses="block">5. Limitación de Responsabilidad</Text>
            <Text fontSize={18}>5.1. Utiliza nuestro sitio web y servicios bajo su propio riesgo. No garantizamos la disponibilidad continua, la precisión o la seguridad del sitio.</Text>
            <Text fontSize={18}>5.2. No somos responsables de ningún daño directo, indirecto, incidental, consecuente o especial que surja del uso de nuestro sitio web o servicios.</Text>
        </article>

        <article>
            <Text fontSize={28} fontWeight="bold" layoutComponentClasses="block">6. Cambios en los Términos</Text>
            <Text fontSize={18}>6.1. Nos reservamos el derecho de modificar estos términos y condiciones en cualquier momento. Los cambios entrarán en vigencia inmediatamente después de su publicación en este sitio web.</Text>
        </article>

        <article>
            <Text fontSize={30} fontWeight="bold" layoutComponentClasses="block">7. Ley Aplicable</Text>
            <Text fontSize={18}>7.1. Estos términos y condiciones se rigen por las leyes de Argentina, y cualquier disputa se resolverá ante los tribunales competentes de Argentina.</Text>
        </article>

        <article className="grid gap-7">
            <div className="grid gap-1">
                <Text fontSize={18}>Por favor, póngase en contacto con nosotros si tiene alguna pregunta o inquietud sobre estos términos y condiciones.</Text>
                <Text fontSize={18}>BeWise SRL</Text>
                <Text fontSize={18}>hola@bewise.com.es</Text>
            </div>
            <Text fontSize={18} fontWeight="bold">Fecha de la última actualización: Octubre 2023</Text>
        </article>

      </section>
    </CustomLayout>
  )
}

export default Terms
