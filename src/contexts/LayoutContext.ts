import { createContext } from "react";

type LayoutContextType = {
  searchParams?: string[];
};

const LayoutContext = createContext<LayoutContextType>({
  searchParams: undefined,
});

export default LayoutContext;
