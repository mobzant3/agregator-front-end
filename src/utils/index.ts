import { CategoriesItem, Product, filterParams, filterPrice } from "@/types";

export const filterProductsByPrice = (
  products: Product[],
  { min, max }: filterPrice
) => {
  return products.filter(({ price_number }) => {
    if (min && price_number < min) {
      return false;
    } else if (max && price_number > max) {
      return false;
    } else {
      return true;
    }
  });
};

export const filterProductsByColor = (
  products: Product[],
  color: Set<string>
) => {
  // Filter products by variants containing at least one color in the array colorFilter.
  return products.filter(({ variants }) => {
    return variants.some((variant) => color.has(variant.color));
  });
};

export const filterProducts = (
  products: Product[],
  filterParams: filterParams
) => {
  let filteredProducts = products;

  if (filterParams.color.size > 0) {
    filteredProducts = filterProductsByColor(
      filteredProducts,
      filterParams.color
    );
  }

  if (filterParams.price.min || filterParams.price.max) {
    filteredProducts = filterProductsByPrice(
      filteredProducts,
      filterParams.price
    );
  }

  return filteredProducts;
};

export const toLinkFormat = (text: string) => {
  return text
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .replace(/ /g, "-")
    .toLowerCase();
};

// check if the slug has products
export const checkProductsOfSlug = async (slug: string) => {
  const response: boolean = await fetch(
    `http://localhost:3000/products/utils/check-slug-products?slug=${slug}`
  )
    .then((res) => res.json())
    .then((res) => res.data);

  return !!response;
};

// filter array with slugs without products
export const filterSlugsArrayWithoutProducts = async (
  slugs: CategoriesItem[]
) => {
  const slugsWithProducts = Promise.all(
    slugs.map((slug) => checkProductsOfSlug(slug.url))
  ).then((res) => {
    return slugs.filter((slug, index) => res[index]);
  });

  return slugsWithProducts;
};
