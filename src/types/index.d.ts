export interface Product {
  _id: string;
  installments_data: string;
  product_id: number;
  price_short: string;
  price_long: string;
  price_number: number;
  compare_at_price_short: null | string;
  compare_at_price_long: null | string;
  compare_at_price_number: number | null;
  stock: number;
  sku: null;
  available: boolean;
  contact: boolean;
  id: number;
  image: number;
  name: string;
  indexable: boolean;
  shopName: ShopName;
  indexableWords: string[];
  slugCombinations: string[];
  url: string;
  image_url: string;
  variants: Variant[];
  siteId: string;
  tags: string[];
  description: string;
  __v: number;
}

export interface Variant {
  color: string;
  size: string;
  test: string;
}

export interface filterParams {
  price: filterPrice;
  color: Set<string>;
}

export interface filterPrice {
  min?: number | null;
  max?: number | null;
}

export interface CategoriesPrototype {
  [key: string]: CategoriesItem[];
}

export interface CategoriesItem {
  url: string;
  value: string;
}

export interface PromptInfo {
  prompt?: string;
  opinionBody?: string;
  combineBody?: string;
  cleanBody?: string;
}
